//题目网址:
//http://soj.me/1938

//题目分析:
//题目同1194。不多说

#include <iostream>
#include <string>
#include <set>

using namespace std;

string friends;
string senders;

int main()
{
    int n, m, t;
    set<string> names;
    
    cin >> t;
    while (t--) {
        cin >> n >> m;
        int i, j, l;

        for (i = 0; i < n; i++) { 
            cin >> friends;
            for (l = 0; l < friends.length(); l++)
                friends[l] = tolower(friends[l]);
            names.insert(friends);
        }

        for (j = 0; j < m; j++) { 
            cin >> senders;
            for (l = 0; l < senders.length(); l++)
                senders[l] = tolower(senders[l]);
            names.erase(senders);
        }

        cout << names.size() << endl;
        names.clear();
    }
    
    return 0;
}                                 